<div class="modal fade bs-example-modal-sm" id="eliminar_actividad" labelledby="myLargeModalLabel">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
				<h4 class="modal-title" align="center">¿Está seguro de Eliminar la actividad?</h4>
			</div>
			<div class="modal-body" align="center">
				<p>Una vez sea Eliminada, no podrá recuperar la infomación.</p>
			</div>
			<div class="modal-footer">
				<a href="javascript:;" class="btn btn-danger" data-dismiss="modal">No</a>
				<button type="button" class="btn btn-success" ng-click="destruir(true,activitySelected.id_actividad)" data-dismiss="modal">
					Si
				</button>
			</div>
		</div>
	</div>
</div>